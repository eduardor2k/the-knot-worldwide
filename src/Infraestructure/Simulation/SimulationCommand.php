<?php

namespace App\Infraestructure\Simulation;

use App\Domain\Elevator\Elevator;
use App\Service\SimulationService;
use App\Domain\Floor\Floor;
use App\Domain\Sequence\Sequence;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'simulation-command',
    description: 'Add a short description for your command',
)]
class SimulationCommand extends Command
{
    public function __construct(private SimulationService $simulationService, string $name = null)
    {
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addArgument('startTime', InputArgument::OPTIONAL, 'At what hour should this simulation start [0/24]?', 0)
            ->addArgument('duration', InputArgument::OPTIONAL, 'How may hours this simulation should last [1/24]?', 1)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $startTime = $input->getArgument('startTime');
        $duration = $input->getArgument('duration');

        $symfonyStyle = new SymfonyStyle($input, $output);

        $floor0 = $this->createFloor(0);
        $floor1 = $this->createFloor(1);
        $floor2 = $this->createFloor(2);
        $floor3 =  $this->createFloor(3);

        $this->simulationService->setElevators([
            $this->createElevator('A',$floor0),
            $this->createElevator('B',$floor0),
            $this->createElevator('C',$floor0),
        ]);

        $this->simulationService->setSequences([
            $this->createSequence(5*60,9*60*60,  11*60*60, [$floor0], [$floor2]),
            $this->createSequence(10*60,9*60*60,  10*60*60, [$floor0], [$floor2]),
            $this->createSequence(20*60,11*60*60,  18*60*60+20*60*60, [$floor0], [$floor0,$floor1,$floor2,$floor3]),
            $this->createSequence(4*60,2*60*60,  3*60*60, [$floor1,$floor2,$floor3], [$floor0]),
        ]);

        // We get the posix time at 00:00 for $currentPosixTime
        $startPosixTime = mktime($startTime,0,0,
            intval(date("n", time())), intval(date("j", time())), intval(date("Y", time())));

        try {
            $output->writeln('START SIMULATION COMMAND');
            for($runs = 0; $runs <= $duration*60; $runs++){

                $runPosixTime = $startPosixTime + $runs*60;

                $output->writeln(sprintf('Run %s/%s at %s', $runs, $duration*60, date("H:i", $runPosixTime)));
                $this->simulationService->run($runPosixTime);

                $output->writeln($this->simulationService->writeReport());
            }
        } catch (\Throwable $exception) {
            $output->writeln(sprintf('ERROR SIMULATION COMMAND: %s', $exception->getMessage()));

            return Command::FAILURE;
        }

        $output->writeln('SUCCESS SIMULATION COMMAND');

        return Command::SUCCESS;
    }


    protected function createSequence($frameTime, $startTime, $endTime, array $calledFrom, array $destinationFloor): Sequence
    {
        $sequence1 = new Sequence();
        $sequence1->setFrameTime($frameTime);
        $sequence1->setStartTime($startTime);
        $sequence1->setEndTime($endTime);
        $sequence1->setCalledFrom($calledFrom);
        $sequence1->setDestinationFloor($destinationFloor);

        return $sequence1;
    }

    protected function createFloor(int $floorNumber)
    {
        $floor0 = new Floor();
        $floor0->setFloorNumber($floorNumber);
        return $floor0;
    }

    protected function createElevator(string $name, Floor $floor): Elevator
    {
        $elevator1 = new Elevator();
        $elevator1->setFloor($floor);
        $elevator1->setName($name);

        return $elevator1;
    }
}
