<?php

namespace App\Domain\Floor;
class Floor
{
    private ?int $floorNumber = null;

    public function getFloorNumber(): ?int
    {
        return $this->floorNumber;
    }

    public function setFloorNumber(int $floorNumber): self
    {
        $this->floorNumber = $floorNumber;

        return $this;
    }
}
