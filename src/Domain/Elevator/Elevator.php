<?php

namespace App\Domain\Elevator;

use App\Domain\Floor\Floor;

class Elevator
{
    private ?Floor $floor = null;

    private ?string $name = null;

    private ?int $trips = 0;

    public function getFloor(): ?Floor
    {
        return $this->floor;
    }

    public function setFloor(Floor $floor): self
    {
        // if the new floor is the same, the elevator will not move
        if(!$this->floor || $this->floor->getFloorNumber() === $floor->getFloorNumber()){
            $this->floor = $floor;
            return $this;
        }

        $this->floor = $floor;
        $this->trips++;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTrips(): int
    {
        return $this->trips;
    }
}
