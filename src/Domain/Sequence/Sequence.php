<?php

namespace App\Domain\Sequence;

use App\Domain\Elevator\Elevator;
use App\Domain\Floor\Floor;

class Sequence
{
    private ?int $startTime = null;

    private ?int $endTime = null;

    private ?int $frameTime = null;

    private array $calledFrom = [];

    private array $destinationFloor = [];

    public function getStartTime(): ?int
    {
        return $this->startTime;
    }

    public function setStartTime(int $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getEndTime(): ?int
    {
        return $this->endTime;
    }

    public function setEndTime(int $endTime): self
    {
        $this->endTime = $endTime;

        return $this;
    }

    public function getFrameTime(): ?int
    {
        return $this->frameTime;
    }

    public function setFrameTime(int $frameTime): self
    {
        $this->frameTime = $frameTime;

        return $this;
    }

    public function getCalledFrom(): array
    {
        return $this->calledFrom;
    }

    public function setCalledFrom(array $calledFrom): self
    {
        $this->calledFrom = $calledFrom;

        return $this;
    }

    /**
     * @return Floor[]
     */
    public function getDestinationFloor(): array
    {
        return $this->destinationFloor;
    }

    public function setDestinationFloor(array $destinationFloor): self
    {
        $this->destinationFloor = $destinationFloor;

        return $this;
    }

    public function canRun(int $currentPosixTime)
    {
        // We get the posix time at 00:00 for $currentPosixTime
        $todayStartPosixTime = mktime(0,0,0,
            intval(date("n", $currentPosixTime)), intval(date("j", $currentPosixTime)), intval(date("Y", $currentPosixTime)));

        // We get how many seconds have passed for today
        $todayCurrentPosixTime = $currentPosixTime - $todayStartPosixTime;

        // check if sequence is in window of operation
        if($this->getStartTime() > $todayCurrentPosixTime || $this->getEndTime() < $todayCurrentPosixTime){
            return false;
        }

        return ($todayCurrentPosixTime % $this->getFrameTime()) === 0;
    }
}
