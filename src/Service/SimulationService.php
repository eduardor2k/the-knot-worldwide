<?php

namespace App\Service;

use App\Domain\Elevator\Elevator;
use App\Domain\Sequence\Sequence;

class SimulationService
{
    /**
     * @var Elevator[]
     */
    protected array $elevators = [];

    /**
     * @var Sequence[]
     */
    protected array $sequences = [];

    public function run(int $currentPosixTime)
    {
        foreach($this->sequences as $sequence){
            $this->processSequence($sequence, $currentPosixTime);
        }
    }

    protected function processSequence(Sequence $sequence, int $currentPosixTime): void
    {
        if(!$sequence->canRun($currentPosixTime)){
            return;
        }

        foreach ($this->elevators as $elevator){
            // if no elevator is found in the floor to be called, skip to the next one
            if(!in_array($elevator->getFloor(),$sequence->getCalledFrom())){
                continue;
            }

            foreach ($sequence->getDestinationFloor() as $destinationFloor){
                $elevator->setFloor($destinationFloor);
            }
        }
    }

    public function writeReport(): string
    {
        $str = [];
        foreach($this->elevators as $elevator)
            $str[] = sprintf(
                "Elevator %s is in floor %s, total trips %s ",
                $elevator->getName(),
                $elevator->getFloor()->getFloorNumber(),
                $elevator->getTrips()
            );
        return implode(" ",$str);
    }

    /**
     * @return array
     */
    public function getElevators(): array
    {
        return $this->elevators;
    }

    /**
     * @param array $elevators
     */
    public function setElevators(array $elevators): void
    {
        $this->elevators = $elevators;
    }

    /**
     * @return array
     */
    public function getSequences(): array
    {
        return $this->sequences;
    }

    /**
     * @param array $sequences
     */
    public function setSequences(array $sequences): void
    {
        $this->sequences = $sequences;
    }
}