FROM php:8-apache

# install node
RUN curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
RUN apt-get update && apt-get install -y git zip nodejs

# install yarn
RUN npm install --global yarn

RUN docker-php-ext-install bcmath pdo_mysql
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN a2enmod rewrite

CMD ["apache2-foreground"]
