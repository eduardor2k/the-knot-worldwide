<?php

namespace App\tests\Unit;

use App\Domain\Elevator\Elevator;
use App\Domain\Floor\Floor;
use PHPUnit\Framework\TestCase;

class ElevatorTest extends TestCase
{
    public function testTripCountIsZeroOnFirstTrip(): void
    {
        $elevator = new Elevator();
        $elevator->setFloor(new Floor());

        $this->assertEquals($elevator->getTrips(), 0);
    }

    public function testTripCountOnFloorChange(): void
    {
        $floor0 = new Floor();
        $floor0->setFloorNumber(0);
        $floor1 = new Floor();
        $floor1->setFloorNumber(1);

        $elevator = new Elevator();
        $elevator->setFloor($floor0);
        $elevator->setFloor($floor1);
        $elevator->setFloor($floor0);

        $this->assertEquals($elevator->getTrips(), 2);
    }

    public function testTripCountSkipIfFloorNoChange(): void
    {
        $floor0 = new Floor();
        $floor0->setFloorNumber(0);
        $floor1 = new Floor();
        $floor1->setFloorNumber(1);

        $elevator = new Elevator();
        $elevator->setFloor($floor0);
        $elevator->setFloor($floor0);
        $elevator->setFloor($floor0);
        $elevator->setFloor($floor1);
        $elevator->setFloor($floor1);
        $elevator->setFloor($floor1);
        $elevator->setFloor($floor0);
        $elevator->setFloor($floor0);
        $elevator->setFloor($floor0);
        $elevator->setFloor($floor0);

        $this->assertEquals($elevator->getTrips(), 2);
    }
}
