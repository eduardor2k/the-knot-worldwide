<?php

namespace App\tests\Unit;

use App\Domain\Sequence\Sequence;
use PHPUnit\Framework\TestCase;

class SequenceTest extends TestCase
{
    /**
     * @dataProvider provider
     */
    public function testSequenceCanRun(Sequence $sequence, int $epochTime, bool $expectedResult): void
    {
        $this->assertEquals($sequence->canRun($epochTime), $expectedResult);
    }

    public function provider()
    {
        return [
            [
                $this->createSequence(5*60,9*60*60,  11*60*60, [0], [2]),
                10*60*60,
                true
            ],
            [
                $this->createSequence(5*60,9*60*60,  11*60*60, [0], [2]),
                8*60*60,
                false
            ],
            [
                $this->createSequence(10*60,9*60*60,  10*60*60, [0], [2]),
                10*60*60,
                true
            ],
            [
                $this->createSequence(20*60,11*60*60,  18*60*60+20*60*60, [0], [0,1,2,3]),
                12*60*60,
                true
            ],
            [
                $this->createSequence(4*60,2*60*60,  3*60*60, [1,2,3], [0]),
                2*60*60,
                true
            ],
            [
                $this->createSequence(4*60,2*60*60,  3*60*60, [1,2,3], [0]),
                2*63*60,
                false
            ],
        ];
    }

    protected function createSequence($frameTime, $startTime, $endTime, array $calledFrom = [], array $destinationFloor = []): Sequence
    {
        $sequence1 = new Sequence();
        $sequence1->setFrameTime($frameTime);
        $sequence1->setStartTime($startTime);
        $sequence1->setEndTime($endTime);
        $sequence1->setCalledFrom($calledFrom);
        $sequence1->setDestinationFloor($destinationFloor);

        return $sequence1;
    }
}
