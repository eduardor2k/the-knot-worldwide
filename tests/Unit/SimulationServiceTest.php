<?php

namespace App\tests\Unit;

use App\Domain\Elevator\Elevator;
use App\Domain\Floor\Floor;
use App\Domain\Sequence\Sequence;
use App\Service\SimulationService;
use PHPUnit\Framework\TestCase;

class SimulationServiceTest extends TestCase
{
    public function testSimulationEmpty(): void
    {
        $service = new SimulationService();
        $service->run(time());

        $this->assertEquals("", $service->writeReport());
    }

    public function testSimulationOneElevatorToOneFloor(): void
    {
        $floor0 = new Floor();
        $floor0->setFloorNumber(0);

        $floor1 = new Floor();
        $floor1->setFloorNumber(1);

        $elevator = new Elevator();
        $elevator->setFloor($floor0);
        $elevator->setName("A");

        $sequence = new Sequence();
        $sequence->setCalledFrom([$floor0]);
        $sequence->setDestinationFloor([$floor1]);
        $sequence->setStartTime(0);
        $sequence->setEndTime(24*60*60);
        $sequence->setFrameTime(10*60);

        $service = new SimulationService();
        $service->setElevators([$elevator]);
        $service->setSequences([$sequence]);
        $service->run(1683762600); // Wednesday, 10 May 2023 23:50:00

        $this->assertEquals("Elevator A is in floor 1, total trips 1 ", $service->writeReport());
    }

    public function testSimulationOneElevatorToFirstFloorAndBack(): void
    {
        $floor0 = new Floor();
        $floor0->setFloorNumber(0);

        $floor1 = new Floor();
        $floor1->setFloorNumber(1);

        $elevator = new Elevator();
        $elevator->setFloor($floor0);
        $elevator->setName("A");

        $sequence = new Sequence();
        $sequence->setCalledFrom([$floor0]);
        $sequence->setDestinationFloor([$floor1]);
        $sequence->setStartTime(0);
        $sequence->setEndTime(24*60*60);
        $sequence->setFrameTime(10*60);

        $sequence2 = new Sequence();
        $sequence2->setCalledFrom([$floor1]);
        $sequence2->setDestinationFloor([$floor0]);
        $sequence2->setStartTime(0);
        $sequence2->setEndTime(24*60*60);
        $sequence2->setFrameTime(20*60);

        $service = new SimulationService();
        $service->setElevators([$elevator]);
        $service->setSequences([$sequence, $sequence2]);
        $service->run(1683762600); // Wednesday, 10 May 2023 23:50:00 Move to first floor

        $this->assertEquals("Elevator A is in floor 1, total trips 1 ", $service->writeReport());

        $service->run(1683763200); // Thursday, 11 May 2023 0:00:00 Move to floor 0

        $this->assertEquals("Elevator A is in floor 0, total trips 2 ", $service->writeReport());

        $service->run(1683763800); // Thursday, 11 May 2023 0:10:00 Move to first floor again

        $this->assertEquals("Elevator A is in floor 1, total trips 3 ", $service->writeReport());
    }
}
