<?php

namespace App\tests\Unit;

use App\Infraestructure\Simulation\SimulationCommand;
use App\Service\SimulationService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

class SimulationCommandTest extends TestCase
{
    /** @var MockObject */
    private $simulationServiceMock;

    /** @var CommandTester */
    private $commandTester;

    protected function setUp(): void
    {
        $this->simulationServiceMock = $this->createMock(SimulationService::class);

        $application = new Application();
        $application->add(new SimulationCommand($this->simulationServiceMock));
        $command = $application->find('simulation-command');
        $this->commandTester = new CommandTester($command);
    }

    public function testRunComand(): void
    {
        $this->simulationServiceMock->expects(self::once())
            ->method("setElevators");
        $this->simulationServiceMock->expects(self::once())
            ->method("setSequences");
        $this->simulationServiceMock->expects(self::exactly(1441))
            ->method("run");
        $this->simulationServiceMock->expects(self::exactly(1441))
            ->method("writeReport");

        $result = $this->commandTester->execute([
            "startTime" => 0,
            "duration" => 24
        ]);

        $this->assertEquals(Command::SUCCESS, $result);
        $this->assertStringContainsString("SUCCESS SIMULATION COMMAND",$this->commandTester->getDisplay());
    }
}
